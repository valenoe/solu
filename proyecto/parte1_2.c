//no funciona
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//llerrena con letras aleatorias
int rellena(int n, char A[n][n]){
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			A[i][j] = "AGTC"[rand()%4];
		}
	}
 return A[n][n];
}
//imprime la matriz que sea llamada
void imprimir(int n, char A[n][n]){
	int i, j;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			printf("|%c", A[i][j]);
		}
		printf("|\n");
	}
	printf("\n");
}
 //ve que letra es
 //
char letra(int i, int j, char A[i][j]){
	char la_letra;
	if(A[i][j] == 'A'){
		la_letra= 'A';
	}
	if(A[i][j] == 'T'){
		la_letra= 'T';
	}
	if(A[i][j] == 'C'){
		la_letra= 'C';
	}
	if(A[i][j] == 'G'){
		la_letra= 'G';
	}
	return la_letra;
}
//segu la leytra que es, va con cual hay que comparar
char comparado(int i, int j, char A[i][j]){
	
	char comparar_8, la_letra;
	la_letra = letra(i, j, A);
	if( la_letra == 'A'){
		comparar_8 = 'G';
	}
	if(la_letra == 'T'){
		comparar_8 ='C';
	}
	if(la_letra == 'C'){
		comparar_8 = 'T';
	}
	if(la_letra == 'G'){
		comparar_8 = 'A';
	}
	return comparar_8;
}
//segun la letra que es, ve con cual se va a cambiar
char cambio(int i, int j, char A[i][j]){
	char la_letra, cambio;
	
	la_letra = letra(i, j, A);
	if( la_letra == 'A'){
		cambio = 'T';
	}
	if(la_letra== 'T'){
		cambio = 'A';
	}
	if(la_letra== 'C'){
		cambio = 'G';
	}
	if(la_letra == 'G'){
		cambio = 'C';
	}
return cambio;
}
//intenta asignarle un valor a algo que no existe
int si_se_pasa(int n, char genoma_base[n][n], int i, int j){
	if (i<0 || j<0){
		return 'x';
	} 
	else if (i>n || j>n){
		return 'x';
	}
	return genoma_base[i][j];
}
//compara con cada uno de los 8 vecinos las letras que son necesarias
int comparar(int i, int j, char A[5][5]){
		
	char comparar_8, cambio_1;
	int contador_comparar =0;
	
	comparar_8 = comparado(i, j, A);
	cambio_1 = cambio(i, j, A);
	printf("\ncomparar_8 = %c\ncambi_1 = %c", comparar_8, cambio_1);
						
	//~ if(A[i][j] == 'A'){
		//~ comparar_8 = 'G';
	//~ }
	//~ if(A[i][j] == 'T'){
		//~ comparar_8= 'G';
	//~ }
	//~ if(A[i][j] == 'C'){
		//~ comparar_8= 'G';
	//~ }
	//~ if(A[i][j] == 'G'){
		//~ comparar_8= 'G';
	//~ }
		//~ int t, w;
		//~ for(t=(i-1); t<=(i+1); t++ ){
			//~ for(w=(j-1); w<=(j+1); w++){
				//~ //si_se_pasa(A, t, w);
				//~ contador_comparar=0;
				//~ if (A[t][w] == comparar_8){
					//~ contador_comparar++;
				//~ }
			
			//~ }
		
		//~ }
		
		
	// busca los vecinos 
	if(A[i-1][j-1] == comparar_8){
		contador_comparar++;
	}
	if(A[i-1][j] == comparar_8){
		contador_comparar++;
	}
	if(A[i-1][j+1] == comparar_8){
		contador_comparar++;
	}
	if(A[i][j-1] == comparar_8){
		contador_comparar++;
	}
	if(A[i][j+1] == comparar_8){
		contador_comparar++;
	}
	if(A[i+1][j-1] == comparar_8){
		contador_comparar++;
	}
	if(A[i+1][j] == comparar_8){
		contador_comparar++;
	}
	if(A[i+1][j+1] == comparar_8){
		contador_comparar++;
	}
		//impersion creada para comprobar si la cantidda de los vecinos es correcto
	printf("\npara %c en (%d,%d), la letra %c es %d", A[i][j], i, j, comparar_8, contador_comparar);
	if(contador_comparar == 3){
		printf(" || debería cambiar por %c", cambio_1);
	}
	else if(contador_comparar != 3){
		printf(" || no debe cambiar");
	}
	//si son exactamente 3 retorna el cambio
	if (contador_comparar == 3){
		return cambio_1;
	}
	//si no, retorna el mismo
	else{
		return A[i][j];
	}

}

//para cada letra se necesitó una función

//para T
int lados_T(int j, int i, char A[5][5], int letra){
	int k, cont;
	//pide las 4 columnas siguientes
	for(k=j; k<=(j+4); k++){
		if(A[i][k] == A[i][j]){
			cont++;
		}
	}
	//si no encuentra solo 1, retorna una x
	if(cont != 1){
		return 'x';
	}
	//si encuentra solo 1, retorna el mismo
	else{
		return  A[i][j];
	}
}
//para A
int lados_A(int j, int i, char A[5][5], int letra){
	int k, cont;
	
	for(k=i; k>=(i-4); k--){
		if(A[k][j] == A[i][j]){
			cont++;
		}
	}
	if(cont != 1){
		return 'x';
	}
	else{
		return  A[i][j];
	}
}
//para G
int lados_G(int j, int i, char A[5][5], int letra){
	int k, cont;
	
	for(k=i; k<=(i+4); k++){
		if(A[k][j] == A[i][j]){
			cont++;
		}
	}
	if(cont != 1){
		return 'x';
	}
	else{
		return  A[i][j];
	}
}
//para C	
int lados_C(int j, int i, char A[5][5], int letra){
	int k, cont;
	
	for(k=j; k>=(j-4); k--){
		if(A[i][k] == A[i][j]){
			cont++;
		}
	}
	if(cont != 1){
		return 'x';
	}
	else{
		return  A[i][j];
	}
}	

//como es solo la parte de las mutaciones no pide nada al usuario
int main(){
	
	srand(time(0));
	
	int n=5;
	char ADN[n][n]; 
	int i, j;
	
	rellena(n, ADN);
	printf("matriz original\n");
	imprimir(n, ADN);
//se crean las matrices de los 2 tipos de mutaciones 
	char temporal_alrededor[n][n];
	char temporal_4lados[n][n];
	
	for(i=0; i<n; i++){   
		for(j=0; j<n; j++){
			letra(i,j,ADN);
			//printf("%c", comparar(i, j, ADN));
			temporal_alrededor[i][j] = comparar(i, j, ADN);
		}
	}
	//imprime la primera
	printf("\nmatriz comparar 8\n");
	imprimir(n, temporal_alrededor);
	printf("\n");
	
	//se evalua la segunda 
	char letra_1;
	for(i=0; i<n; i++){   
		for(j=0; j<n; j++){
			letra_1 = letra(i, j, ADN);
			if(letra_1=='A'){
				temporal_4lados[i][j] = lados_A(i, j, ADN, letra_1);
			}
			if(letra_1=='T'){
				temporal_4lados[i][j] = lados_T(i, j, ADN, letra_1);
			}	
			if(letra_1=='C'){
				temporal_4lados[i][j] = lados_C(i, j, ADN, letra_1);
			}	
			if(letra_1=='G'){
				temporal_4lados[i][j] = lados_G(i, j, ADN, letra_1);
			}		
		}
	}
	//imprime la segunda
	printf("\n matriz comparar 4\n");
	imprimir(n, temporal_4lados);
	printf("\n");
	
	//aun no las mezcla

	return 0;
}
