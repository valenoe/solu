//no funciona
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//le da la bienveida al programa y le pide el tamaño de la matriz
int inicio(){

	int porte;

	printf("        ·····bienvenido·····\n");
	printf("tendrá que ingresar ciertos datos para poder\nanalizar las muatciones de su gen base\n");
	printf("ingrese el tamaño de la matriz del genoma\n");
	do{
		scanf("%d", &porte);
		if(porte<5){
			printf("el tamaño tiene que ser mayor a 5\ningrese el tamaño: ");
		}
	} while(porte<5);
	return porte;
}

int rellena(int n, char A[n][n]){
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			A[i][j] = "AGTC"[rand()%4];
		}
	}
 return A[n][n];
}

//imprime la matriz que sea llamada
void imprimir(int n, char A[n][n]){
	int i, j;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			printf("|%c", A[i][j]);
		}
		printf("|\n");
	}
	printf("\n");
}

//pide y guarda las generaciones
int inicio_2(){
	
	int generaciones;
	
	printf("¿cuantas generaciones quiere simular?\n");
	scanf("%d", &generaciones);
	
	return generaciones; 
}

char comparado(int i, int j, char A[i][j]){
	
	if(A[i][j] == 'A'){
		return ('G');
	}
	if(A[i][j] == 'T'){
		return ('C');
	}
	if(A[i][j] == 'C'){
		return ('T');
	}
	if(A[i][j] == 'G'){
		return ('A');
	}
}

//segun la letra que es, ve con cual se va a cambiar
char cambio(int i, int j, char A[i][j]){
	
	if(A[i][j] == 'A'){
		return ('T');
	}
	if(A[i][j]== 'T'){
		return ('A');
	}
	if(A[i][j]== 'C'){
		return ('G');
	}
	if(A[i][j] == 'G'){
		return ('C');
	}
}

int comparar(int i, int j, int n, char A[n][n]){
		
	char comparar_8, cambio_1;
	int contador_comparar =0;
	
	comparar_8 = comparado(i, j, A);
	cambio_1 = cambio(i, j, A);

	
	if(A[i-1][j-1] == comparar_8){
		contador_comparar++;
	}
	if(A[i-1][j] == comparar_8){
		contador_comparar++;
	}
	if(A[i-1][j+1] == comparar_8){
		contador_comparar++;
	}
	if(A[i][j-1] == comparar_8){
		contador_comparar++;
	}
	if(A[i][j+1] == comparar_8){
		contador_comparar++;
	}
	if(A[i+1][j-1] == comparar_8){
		contador_comparar++;
	}
	if(A[i+1][j] == comparar_8){
		contador_comparar++;
	}
	if(A[i+1][j+1] == comparar_8){
		contador_comparar++;
	}					
		//~ int t, w;
		//~ for(t=(i-1); t<=(i+1); t++ ){
			//~ for(w=(j-1); w<=(j+1); w++){
				//~ //si_se_pasa(A, t, w);
				//~ contador_comparar=0;
				//~ if (A[t][w] == comparar_8){
					//~ contador_comparar++;
				//~ }
			//~ }
		//~ }
		
	//impersion creada para comprobar si la cantidda de los vecinos es correcta
	printf("\npara %c en (%d,%d), la letra %c es %d", A[i][j], i, j, comparar_8, contador_comparar);
	printf("\ncomparar_8 = %c\ncambi_1 = %c", comparar_8, cambio_1);
	if(contador_comparar == 3){
		printf(" || debería cambiar por %c", cambio_1);
	}
	else if(contador_comparar != 3){
		printf(" || no debe cambiar");
	}
	//si son exactamente 3 retorna el cambio
	if (contador_comparar == 3){
		return cambio_1;
	}
	//si no, retorna el mismo
	else{
		return A[i][j];
	}

}

int lados_T(int j, int i, int n, char A[n][n]){
	int k, cont=0;
	//pide las 4 columnas siguientes
	for(k=j; k<(j+5); k++){
		if(A[i][k] == A[i][j]){
			cont++;
		}
	}
	printf("\neste es el contador de T: %d", cont);
	return cont;
}
//para A
int lados_A(int j, int i, int n, char A[n][n]){
	int k, cont=0;
	
	for(k=i; k>=(i-4); k--){
		if(A[k][j] == A[i][j]){
			cont++;
		}
	}
	printf("\neste es el contador de A: %d", cont);
	return cont;
}
//para G
int lados_G(int j, int i,int n, char A[n][n]){
	int k, cont=0;
	
	for(k=i; k<=(i+4); k++){
		if(A[k][j] == A[i][j]){
			cont++;
		}
	}
	printf("\neste es el contador de G: %d", cont);
	return cont;
}

//para C	
int lados_C(int j, int i, int n, char A[n][n]){
	int k, cont=0;
	
	for(k=j; k>=(j-4); k--){
		if(A[i][k] == A[i][j]){
			cont++;
		}
	}
	printf("\neste es el contador de C: %d", cont);
	return cont;
}	

char filas_y_columnas(int i, int j, int n, char a[n][n]){
	
	int contador_filas_y_columnas = 0;
	//lama a las funciones de que cuentan las filas o columnas de cada letra
	if(a[i][j] == 'A'){
		contador_filas_y_columnas = lados_A(j, i, n, a);
	}
	else if(a[i][j] == 'T'){
		contador_filas_y_columnas = lados_T(j, i, n, a);
	}
	else if(a[i][j] == 'C'){
		contador_filas_y_columnas = lados_C(j, i, n, a);
	}
	else if(a[i][j] == 'G'){
		contador_filas_y_columnas = lados_G(j, i, n, a);
	}
	
	if(contador_filas_y_columnas == 1){
		return a[i][j];
	}
	else{
		return  'x';
	}
}

//ls une
char pr_segund(int i, int j, int n, char genoma_base[n][n]){
	
	char temporal_alrededor[n][n];
	char temporal_4lados[n][n];

	for(i=0; i<n; i++){   
		for(j=0; j<n; j++){
			temporal_4lados[i][j] = filas_y_columnas(i, j, n, genoma_base);	
		}
	}

	
for(i=0; i<n; i++){   
		for(j=0; j<n; j++){
			//printf("%c", comparar(i, j, ADN));
			temporal_alrededor[i][j] = comparar(i, j, n, genoma_base);
		}
	}
	
}
 

//probavilidades de cada letra en la matriz
int evaluar(int N, char genoma_base[N][N], char letra){
	
	int contador_letra=0, i, j;
	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			//esta_si_evalua(i, j, genoma_base);
			if(genoma_base[i][j] == letra){
				contador_letra++;
			}
		}//llamar a la letra a consulatr en la funcion principal
	}
		
	return contador_letra;
}

int main(){
	//inicializa semilla del tiempo
	srand(time(0));
	
	int n, i, j;
	
	//llama a funcion que pide el rango de la matriz
	n = inicio();

	char genoma_base[n][n]; 
	//base de todo
	rellena(n, genoma_base);
	printf("matriz original\n");
	imprimir(n, genoma_base);
	
	//se crean las matrices de los 2 tipos de mutaciones 
	
	//imprime la primera
	printf("\nmatriz comparar 8\n");
	imprimir(n, temporal_alrededor);
	printf("\n");
	
	//se evalua la segunda 
	for (i=0; i<n; i++){
		for(j=0; j<n; j++){
			pr_segund(i, j, n, genoma_base);
		}
	}
	//imprime la segunda
	printf("\n matriz comparar 4\n");
	imprimir(n, temporal_4lados);
	printf("\n");
	
	
	//CICLO FINL
	for(i=0; i<n; i++){   
		for(j=0; j<n; j++){
            
	
	char letra[4] = {'A','T','C','G'};
	int cont;
	//funcion que cuenta cada letra de la matriz 
	for(i=0; i<4; i++){
		cont = evaluar(n, genoma_base, letra[i]);
		printf("\nel contador de %c es %d\n", letra[i], cont);
		printf("la probabilidad de que salga %c en el siguente genoma es %d/%d", letra[i], cont, n*n);
	}
	
	return  0;
}
