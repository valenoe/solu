#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	
	srand(time(0));
	
	int i, j, ATCG;
	int N=5; 
	char genoma_base[N][N], nucle;
	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			 ATCG= (rand()%4);
			 	switch(ATCG){
					case 0: 
						nucle = 'a'; 
						genoma_base[i][j] = nucle;
						break;
					case 1: 
						nucle = 't';
						genoma_base[i][j] = nucle;
						break;
					case 2:
						nucle = 'c';
						genoma_base[i][j] = nucle;
						break;
					case 3:
						nucle = 'g';
						genoma_base[i][j] = nucle;
						break;
					}
			printf("|%d", genoma_base[i][j]);
		}
		printf("|\n");
	}
	return 0;
}

