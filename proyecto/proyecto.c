#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>//se va a ocupar despues

//imprime la matriz que sea llamada
void imprimir(int n, char A[n][n]){
	int i, j;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			printf("|%c", A[i][j]);
		}
		printf("|\n");
	}
	printf("\n");
}

//funcion para que cuando encuantre una i o j que no exista ponga una x
int si_se_pasa(int N, char genoma_base[N][N], int i, int j){
	if (i<0 || j<0){
		return 'x';
	} 
	else if (i>N || j>N){
		return 'x';
	}
	return genoma_base[i][j];
}

int evaluar(int N, char genoma_base[N][N], char letra){
	
	int contador_letra=0, i, j;
	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			//esta_si_evalua(i, j, genoma_base);
			if(genoma_base[i][j] == letra){
				contador_letra++;
			}
		}//llamar a la letra a consulatr en la funcion principal
	}
	
	
	return contador_letra;
	}
	
int menu_1(){//funciona hasta el asterisco
	
	printf("si quiere cambiar el tamaño de la matriz\nAPRETE 1\n");
	printf("si quiere cambiar la secuencia a buscar\nAPTETE 2\n");
	printf("si quiere cambiar el número de generaciones\n(solo va a poder cambiar a un número más grande)\nAPRETE 3\n");
	printf("si no quiere hacer nada y continuar como está\nAPRETE 4\n");
	printf("si quiere salir del programa\nAPRETE 0\n");
	int quiere;
	scanf("%d", &quiere);//esto tiene que ir abajo en main
	return quiere;
	
//ver que pasa con cada numero
}
int rellena(int n, char A[n][n]){
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			A[i][j] = "AGTC"[rand()%4];
			
		}
	}
 return A[n][n];
 
}




int inicio(){
	int porte;
	printf("        ·····bienvenido·····\n");
	printf("tendrá que ingresar ciertos datos para poder\nanalizar las muatciones de su gen base\n");
	printf("ingrese el tamaño de la matriz del genoma\n");
	do{
		scanf("%d", &porte);
		if(porte<5){
			printf("el tamaño tiene que ser mayor a 5\ningrese el tamaño: ");
		}
	} while(porte<5);
	return porte;
}

int inicio_2(){
	
	int generaciones;
	
	printf("¿cuantas generaciones quiere simular?\n");
	scanf("%d", &generaciones);
	
	return generaciones; 
}

int main(){
	//inicializa se,illa del tiempo
	srand(time(0));
	
	int N, i;
	
	//llama a funcion que pide el rango de la matriz
	N = inicio();

	char genoma_base[N][N]; 
	//base de todo
	rellena(N, genoma_base);
	
	char letra[4] = {'A','T','C','G'};
	int cont;
	//funcion que cuenta cada letra de la matriz 
	for(i=0; i<4; i++){
		cont = evaluar(N, genoma_base, letra[i]);
		printf("\nel contador de %c es %d\n", letra[i], cont);
		printf("la probabilidad de que salga %c en el siguente genoma es %d/%d", letra[i], cont, N*N);
	}
	
	
	
	
	return  0;
}
