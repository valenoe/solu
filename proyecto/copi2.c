//no funciona
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
//le da la bienveida al programa y le pide el tamaño de la matriz
int inicio(){

	int porte;

	printf("        ·····bienvenido·····\n");
	printf("tendrá que ingresar ciertos datos para poder\nanalizar las muatciones de su gen base\n");
	printf("ingrese el tamaño de la matriz del genoma\n");
	do{
		scanf("%d", &porte);
		if(porte<5){
			printf("el tamaño tiene que ser mayor a 5\ningrese el tamaño: ");
		}
	} while(porte<5);
	return porte;
}

char rellena(int n, char A[n][n]){
	for(int i=0; i<n; i++){
		for(int j=0; j<n; j++){
			A[i][j] = "AGTC"[rand()%4];
		}
	}
 return A[n][n];
}

//imprime la matriz que sea llamada
void imprimir_mutada(int n, char A[n][n]){
	int i, j;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			printf("|%c", A[i][j]);
		}
		printf("|\n");
	}
	printf("\n");
}
void imprimir_base(int G, int n, char A[G][n][n]){
	int i, j, l;
	for(l=0; l<G; l++){
		for(i=0; i<n; i++){
			for(j=0; j<n; j++){
				printf("|%c", A[l][i][j]);
			}
			printf("|\n");
		}
		printf("\n");
	}
}

//pide y guarda las generaciones
int inicio_2(){
	
	int generaciones;
	
	printf("¿cuantas generaciones quiere simular?\n");
	scanf("%d", &generaciones);
	
	return generaciones; 
}

char comparado(int i, int j, char A[i][j]){
	
	if(A[i][j] == 'A'){
		return ('G');
	}
	if(A[i][j] == 'T'){
		return ('C');
	}
	if(A[i][j] == 'C'){
		return ('T');
	}
	if(A[i][j] == 'G'){
		return ('A');
	}
}

//segun la letra que es, ve con cual se va a cambiar
char cambio(int i, int j, char A[i][j]){
	
	if(A[i][j] == 'A'){
		return ('T');
	}
	if(A[i][j]== 'T'){
		return ('A');
	}
	if(A[i][j]== 'C'){
		return ('G');
	}
	if(A[i][j] == 'G'){
		return ('C');
	}
}

char comparar(int i, int j, int n, char A[n][n]){
		
	char comparar_8, cambio_1;
	int contador_comparar =0;
	
	comparar_8 = comparado(i, j, A);
	cambio_1 = cambio(i, j, A);
	
	if(A[i-1][j-1] == comparar_8){
		if(i-1>0 && j-1>0){
			contador_comparar++;}
	}
	if(A[i-1][j] == comparar_8){
		if(i-i > 0){
			contador_comparar++;}
	}
	if(A[i-1][j+1] == comparar_8){
		if(i-1>0 && j+1<n){
			contador_comparar++;}
	}
	if(A[i][j-1] == comparar_8){
		if(j-1>0){
			contador_comparar++;}
	}
	if(A[i][j+1] == comparar_8){
		if(j+1<n){
		contador_comparar++;}
	}
	if(A[i+1][j-1] == comparar_8){
		if(i+1<n && j-1>0){
			contador_comparar++;}
	}
	if(A[i+1][j] == comparar_8){
		if(i+1<n){
			contador_comparar++;}
	}
	if(A[i+1][j+1] == comparar_8){
		if(i+1<n && j+1<n){
			contador_comparar++;}
	}					
	//impersion creada para comprobar si la cantidda de los vecinos es correcta
	//printf("\npara %c en (%d,%d), la letra %c es %d", A[i][j], i, j, comparar_8, contador_comparar);
	//printf("\ncomparar_8 = %c\ncambi_1 = %c", comparar_8, cambio_1);
	if(contador_comparar == 3){
		//printf(" || debería cambiar por %c", cambio_1);
	}
	else if(contador_comparar != 3){
		//printf(" || no debe cambiar");
	}
	//si son exactamente 3 retorna el cambio
	if (contador_comparar == 3){
		return cambio_1;
	}
	//si no, retorna el mismo
	else{
		return A[i][j];
	}

}

int lados_T(int j, int i, int n, char A[n][n]){
	int k, cont=0;
	//pide las 4 columnas siguientes
	for(k=j; k<(j+5); k++){
		if(A[i][k] == A[i][j]){
			cont++;
		}
	}
	//printf("\neste es el contador de T: %d", cont);
	return cont;
}
//para A
int lados_A(int j, int i, int n, char A[n][n]){
	int k, cont=0;
	
	for(k=i; k>=(i-4); k--){
		if(A[k][j] == A[i][j]){
							cont++;
			}
		}
	//printf("\neste es el contador de A: %d", cont);
	return cont;
}
//para G
int lados_G(int j, int i,int n, char A[n][n]){
	int k, cont=0;
	
	for(k=i; k<=(i+4); k++){
		if(A[k][j] == A[i][j]){
			cont++;
		}
	}
	//printf("\neste es el contador de G: %d", cont);
	return cont;
}

//para C	
int lados_C(int j, int i, int n, char A[n][n]){
	int k, cont=0;
	
	for(k=j; k>=(j-4); k--){
		if(A[i][k] == A[i][j]){
			cont++;
		}
	}
	//printf("\neste es el contador de C: %d", cont);
	return cont;
}	

char filas_y_columnas(int i, int j, int n, char a[n][n]){
	
	int contador_filas_y_columnas = 0;
	//lama a las funciones de que cuentan las filas o columnas de cada letra
	if(a[i][j] == 'A'){
		contador_filas_y_columnas = lados_A(j, i, n, a);
	}
	else if(a[i][j] == 'T'){
		contador_filas_y_columnas = lados_T(j, i, n, a);
	}
	else if(a[i][j] == 'C'){
		contador_filas_y_columnas = lados_C(j, i, n, a);
	}
	else if(a[i][j] == 'G'){
		contador_filas_y_columnas = lados_G(j, i, n, a);
	}
	
	if(contador_filas_y_columnas == 1){
		return a[i][j];
	}
	else{
		return  'x';
	}
}

//ls une
char pr_segund(int i, int j, int n, char genoma_base[n][n]){
	
	char temporal_alrededor[n][n];
	char temporal_4lados[n][n];

	//printf("%c", comparar(i, j, ADN));
	temporal_alrededor[i][j] = comparar(i, j, n, genoma_base);
		//printf("temp_alrededor: %c", temporal_alrededor[i][j]);

	char letra, sobra, se_devuelve=0;
	
	letra= cambio(i, j, genoma_base);
	
	if(temporal_alrededor[i][j] == letra){
		se_devuelve = temporal_alrededor[i][j];
	//printf("\nesto se devuelve %c en(%d,%d) y temporl , %c", se_devuelve, i, j, temporal_alrededor[i][j]);
	}
	else if(temporal_4lados[i][j] != genoma_base[i][j]){
		
		temporal_4lados[i][j] = filas_y_columnas(i, j, n, genoma_base);	
		//	printf("\ntemp_4-lados: %c en (%d, %d)", temporal_4lados[i][j], i, j);
		
		if(genoma_base[i][j] == temporal_4lados[i][j]){
			se_devuelve = genoma_base[i][j];
		}
		else {
			sobra = "ATGC"[rand()%4];
			se_devuelve = sobra;
		}
	}
	//printf("\nesto se devuelve %c", se_devuelve);
	return se_devuelve;
}
 
int main(){
	//inicializa semilla del tiempo
	srand(time(0));
	
	int n, i, j, l;
	
	//llama a funcion que pide el rango de la matriz
	n = inicio();
	
	int G;
	G = inicio_2();
	char mutacion[G][n][n];
	char genoma_base[G][n][n]; //->temporal
	//base de todo
	rellena(n, genoma_base[0]);
	printf("base de generaciones(padre sin mutar)\n");
	imprimir_mutada(n, genoma_base[0]);	
	
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			mutacion[0][i][j] = pr_segund(i, j, n, genoma_base[0]);
		}
	}
	printf("primera generacion\n");
			imprimir_mutada(n, mutacion[0]);
//imprime las generaciones creadas aleatoriamente 
	for(l=1; l<G; l++){
		for(i=0; i<n; i++){
			for(j=0; j<n; j++){
				genoma_base[l][i][j] = mutacion[0][i][rand()%(n)];
				}	
			}
		for(i=0; i<n; i++){
			for(j=0; j<n; j++){	
				mutacion[l][i][j] = pr_segund(i, j, n, genoma_base[l]);
			}
		}		
			printf("\ngeneracion base %d sin mutar\n", l+1);
			imprimir_mutada(n, genoma_base[l]);
			printf("\n");
			printf("\ngeneracio mutada %d\n", l+1);
			imprimir_mutada(n, mutacion[l]);
			printf("\n");
	}
	return 0;
}
