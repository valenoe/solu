#include <stdio.h>

 int main(){
		//productos: A
		//pedidos: B
		//stock: C
		int A [10] = {41, 5, 12, 6, 8, 50, 8, 9, 32, 7};
		int B [10] = {8, 4, 16, 24, 12, 9, 31, 21, 14, 15};
		int C [10], i;
		//compara A y B para rellenar C
		for(i=0; i<10; i++){
			if(A[i] == B[i]){ //si son iguales, se guarda igual que A o B
				C[i] = A[i];
			}
			else if(B[i] > A[i]){ //si B es mayor que A, se guarda el doble de la diferencia entre B y A 
				C [i] = 2*(B[i] - A[i]);
			}
			else if (A[i] > B[i]){ //si A es mayor, se guarda B
				C[i] = B[i];
			}
		}
		//imprime A
		printf("productos: ");
		for(i=0; i<10; i++){
			printf(" %d ", A[i]);
		}
		//imprime B
		printf("\npedidos: ");
		for(i=0; i<10; i++){
			printf(" %d ", B[i]);
		}
		//imprime C
		printf("\nlo que se necesita para mantener stock: ");
		for(i=0; i<10; i++){
			printf(" %d ", C[i]);
		}
		return 0;
	}
