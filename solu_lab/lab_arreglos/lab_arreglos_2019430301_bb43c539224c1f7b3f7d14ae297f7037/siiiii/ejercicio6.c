#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	
	srand(time(0)); //define la semilla del tiempo
	int arr[10], i, NUMERO;
	
	//pide el numero para comparar
	printf("ingrese un numero a comparar: ");
	scanf("%d", &NUMERO);
	
	//el ciclo llena el arreglo con numeros aleatorios y compara cada uno con NUMERO e imprime el indice donde está el número que calse
	for(i=0; i<10; i++){      
		arr[i] = rand() % 10; 
		if(arr[i] == NUMERO){      
			printf("\nel numero en la posición %d es igual a %d", i, NUMERO);
		}
		//también imprime las posiciones que no son iguales a NUMERO 
		else{
			printf("\nno hay nueros iguales a %d en la posición %d", NUMERO, i);
		}
	}
	return 0;
}
	
	
	
	
