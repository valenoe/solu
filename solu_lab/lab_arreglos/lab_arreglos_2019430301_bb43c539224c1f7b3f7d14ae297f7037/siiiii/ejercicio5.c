#include <stdio.h>

int prim( int num){
	
	int i, primo=0;
	//se evalua que los numeros no sean 0 o 1
	if(num == 1){
		return 0;
	}
	else if(num == 0){
		return 0;
	}
	//si avanza, busca los divisores de cada numeros para sumarlos
	else{ 
		for(i=1; i<100; i++){
			if(num % i == 0){
				primo = primo + (num/i);
			}
		}
	}//si la suma de los divisores es igual al numero más 1
		if(primo == num+1){
			return num; // devuelve el número
		}
		else{
			return  0;
		}
	}

	

int main(){
	
	int i, primos[100];
	
	printf("numeros primos del 1 al 100:\n");
	// se evaluan los numeros del 1 al 100  
	for(i=100; i>0; i--){ 
		if(prim(i) == i){     //se evalua que i sea igual al numero que devuelve la funcion 
			primos[i]= prim(i); //para agregarlos al arreglo primos
			printf("-%d-", primos[i]); //que luego se imprime
		}
	}
	
	return 0;
}
