#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	
	int i; 
	
	printf("arreglo de 0 sa 9 espacios");
	int arri[10];
	
	//se pide el primer número del arreglo
	printf("\ningrese un numero: \n"); 
	scanf("%d", &arri[0]);
	
	printf("el número en la posicion 0 es %d\n", arri[0]);
	
	//se pide el resto comparandolo con el primer numero
	i=1;
	while(i<10){
		printf("Ingrese un número para elemento %d: \n", i);
		scanf("%d", &arri[i]);
		
		//si el número ingresado es mayor al anterior, el indice avanza 
		if(arri[i] > arri[i-1]){
			i++;
		}
		//si no es mayor, lo vuelve a pedir 
		else{
			printf("Debe ser un numero mayor que %d\n", arri[i-1]);
		}
	}
	//imprime el arreglo completo
	for (i=0; i<10; i++){
		printf(" %d ", arri[i]);
	}
	
		return 0;
}
