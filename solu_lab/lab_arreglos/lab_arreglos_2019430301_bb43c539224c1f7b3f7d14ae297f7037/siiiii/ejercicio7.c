#include <stdio.h>


int main(){
	
	int j, z, i, parti[10], partidario=0;
	
	//le pide al usuario que ingrese numeros al arreglo
	printf("arreglo de 10 digitos ingresados por usted: \n");
	for(i=0; i<10; i++){    
		scanf("%d", &parti[i]); 
	}
	
	for(j=0; j<10; j++){   //compara los de la posicion par con la posicion impar
		for(z=1; z<10; z++){
			if (parti[j] > parti[z]){//si los de la posición par son mayores a la posicion impar  
				partidario++;//se suma el contador llamado partidario
			}
			
			z++;
		}
		j++;
	}
	//solo si el contador llega a 25, el número va a ser partidario
	//25 significa que todos los numeros de los indices pares son mayores a los de los indices impares
	if(partidario == 25){ 
		printf("es partidario\n|");
		for(i=0; i<10; i++){
			printf(" %d |", parti[i]);
		}
	}// cualquier numero menor significa que uno o más numeros de los indices pares es menor o igual a otros de indice impar
	else if(partidario < 25){
		printf("\nno es partidario");
	}
	
	
		return 0;
		
	}
	
	
	
