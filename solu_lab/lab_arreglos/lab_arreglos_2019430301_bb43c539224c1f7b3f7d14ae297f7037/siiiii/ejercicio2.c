#include <stdio.h>

int main(){
	
	int numeros[10], i, j, contador_impares=0;   
	
	//ciclo que le da números al arreglo
	for(j=0; j<10; j++){
		numeros[j]=j;
		}
		
	printf("números originales= ");
	//se van a imprimir los números de este arreglo
	for(j=0; j<10; j++){
		printf(" %d ", numeros[j]);
	}
	//se evalua que sean impares para definir el tamaño del nuevo arreglo con contador_impares
	for(i=0; i<10 ; i++){
		if(numeros[i]%2!=0){
		contador_impares++;	
		}
	}
	//imprime la cantidad de impares que existen
	printf("\ncontador de impares: %d", contador_impares); 
	//define el nuevo arreglo con el tamaño de contador_impares
	int impares[contador_impares];
	int l=0;
	//este ciclo le agrega los numeros impares al nuevo arreglo
	for(i=0; i<10 ; i++){
		if(numeros[i]%2!=0){
			impares[l]= numeros[i];
			l++;
		}
	}
	//se imprime el nuevo arreglo
	printf("\n arreglo impares: ");
	for(i=0; i<contador_impares; i++){
		printf(" %d ", impares[i]);
	}	
	
	return  0;
}
