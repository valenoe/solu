#include <stdio.h>

int main(){
	
	int i, X, A, j;
	
	printf("ingrese un numero que defena la cantidad de datos del arreglo: ");// se le pide al rango al usuario
	scanf("%d", &A);
	printf("las posiciones empiezan desde 0\n");
	
	int caos[A];//se define el arreglo caos con ese rango
	
	printf("ingrese un numero a comparar: ");//le pide en número para comparar
	scanf("%d", &X);
	
	for(i=0; i<A; i++){
		printf("ingerse numeros al arrreglo en la posición %d: ", i);//se le piden números para rellenar el arreglo
		scanf("%d", &caos[i]);
		
	}
	printf("arreglo total: ");
	for(i=0; i<A; i++){
		printf(" %d ", caos[i]);
	}
	
	int mayorX = 0;
	int menorX = 0;
	int igualX = 0;
	//ciclo que compara: 
	for(j=0; j<A; j++){
		if(caos[j] < X){ //cuantos numeros son menor a X
			menorX++;
		}
		if(caos[j] > X){ // cuantos son mayores a X
			mayorX++;
		}
		if(caos[j] == X){ // cuantos son iguales a X
			igualX++;
		}
	}
	//imprime la cantidad de cada tipo
	printf("\nen comparación a %d hay:\n%d numeros mayores\n%d numeros menores\n%d numeros iguales\n", X, mayorX, menorX, igualX);
	//se definen arreglos y contadores
	int arr_menores[menorX];
	int arr_mayores[mayorX];
	int arr_iguales[igualX];
	int jmayor = 0;
	int jmenor = 0;
	int jigual = 0;
	
	//ciclo que evalúa que número es mayor, menor o igual a X
	//lo guarda en sus respectivos arreglos
	for(int l=0; l<A; l++){
		if(caos[l] > X){
			arr_mayores[jmayor] = caos[l];
			jmayor++;
		}
		if(caos[l] < X){
			arr_menores[jmenor] = caos[l];	
			jmenor++;
		}
		if(caos[l] == X){
			arr_iguales[jigual] = caos[l];
			jigual++;
		}
	}
	printf("\nmayores: ");//imprime los numeros mayores a X
	for(i=0; i<mayorX; i++){
		printf(" %d ", arr_mayores[i]);
	}
	printf("\nmenores: ");//imprime los menores a X
	for(i=0; i<menorX; i++){
		printf(" %d ", arr_menores[i]);
	}
	printf("\niguales: "); // imprime loa iguales a X
	for(i=0; i<igualX; i++){
		printf(" %d ", arr_iguales[i]);
	}
	
	printf("\n");
	for(i=0; i<A; i++){//prueba si los numeros del arreglo son dividibles por el pedido originalmente
		if(caos[i]%X==0){
			printf("\nel número %d es multiplo de %d en la posición %d ", caos[i], X, i);// si es que sí, los imprime
		}
		else{
			printf("\nno hay multiplos de %d en la posicion %d", X, i);//si no, imprime la posicion de cual no es multiplo
		}
	}	 
	
	return  0;
}
