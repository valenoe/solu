#include<stdio.h>

int main(void) {
  int a[3][3]={{1,2,4},{3,4,2},{5,6,3}};
  int b[3][3]={{4,-5,2},{1,2,0},{3,4,-3}};
  int i, j,k,temporal;
  
  printf("\n Matriz A \n");
  
  for (i=0; i<3; i++){
    for (j=0; j<3; j++){
      printf("|%d", a[i][j]);
    }
    printf("|\n");
  }
  printf("\n Matriz B \n");
  for (i=0; i<3; i++){
    for (j=0; j<3; j++){
      printf("|%d", b[i][j]);
    }
    printf("|\n");
  }

  printf("\n Matriz C (Multiplicación) \n");
  
  for (i=0; i<3; i++){
    for (j=0; j<3; j++){ // k para las columnas de la matriz resultante
       temporal = 0 ;
        for (k = 0 ; k < 3 ; k++ ){                      
            temporal = temporal + a[i][j] * b[j][k];
        }
        printf("|%d", temporal);
    }
    printf("|\n");
}
  return 0;
}
