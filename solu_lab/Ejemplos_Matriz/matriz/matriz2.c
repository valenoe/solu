#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(void) {
  
  srand(time(0));
  
  int a[50][50];
  int i, j;
  
  for (i=0; i<50; i++){
    for (j=0; j<50; j++){
      a[i][j]=rand()%10;
    }
  }
  printf("\n\n original \n\n");
  
  for (i=0; i<50; i++){
    for (j=0; j<50; j++){
		printf("|%d", a[i][j]);
	}
    printf("|\n");
  }
  
  printf("\n\n diagonal \n\n");
  
  
  for (i=0; i<50; i++){
    for (j=0; j<50; j++){
	  if (i==j || (i == (50-j)-1))
		printf("|%d", a[i][j]);
	  else
	    printf("|0");
    }
    printf("|\n");
  }

  return 0;
}
