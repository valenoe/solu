#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(void) {
  
  srand(time(0));
  
  int a[10][10];
  int i, j, randA, randB, repeticiones=0;
  
  for (i=0; i<10; i++){
    for (j=0; j<10; j++){
      a[i][j]=rand()%9 + 1;
    }
  }
  printf("\n\n original \n\n");
  
  for (i=0; i<10; i++){
    for (j=0; j<10; j++){
		printf("|%d", a[i][j]);
	}
    printf("|\n");
  }


  for(i=0; i<10;i++){
	  randA=rand()%10;
	  randB=rand()%10;
	
	  if(a[randA][randB]>0)
		a[randA][randB] = 0;
	  else
		repeticiones = repeticiones +1 ;
  }

  printf("\n\n modificada \n\n");
  
  
  for (i=0; i<10; i++){
    for (j=0; j<10; j++){
		printf("|%d", a[i][j]);
	}
    printf("|\n");
  }
  
  printf("Repeticiones : %d \n",repeticiones);


  return 0;
}
