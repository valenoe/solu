#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){
	
	char frase[50];//inicializo las funciones
	int largo, i;
	

	 
	printf("ingrese una frase de menos de 50 caracteres (mínimo 2 palabras): \n");  //se pide la frase
	gets(frase); // "gets" para que tome todos los caracteres
	
	largo = strlen(frase);//para saber el largo de la frase
	
	for(i=(largo-1); i>=0; i--){ //se coloca "largo-1" para no empezar por "\0", así si se ve
		printf("%c", frase[i]); // imprime la nueva frase
	}
	
	return  0;//termina la función
}
