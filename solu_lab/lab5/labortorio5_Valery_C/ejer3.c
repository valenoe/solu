#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void espacio(char frase[]){
	
	char espacio= ' ';//se declara el espacio en blanco
	int i;
	
	for(i=0; i<100; i++){
		if(frase[i]==espacio){// para buscarlo en la frase original
			frase[i]= '*';//la posición en que está espacio se cambia por un asterisco
		}
	}
	printf("la frase nueva es: %s", frase);//la imprime la frase nueva
}

int main(){
	
	
	char frase[100]= "esta frase sí";//se declara una frase
	
	espacio(frase);//se llama a la función
	
	return  0;//termina
	
}
