#include <stdio.h>
#include <math.h>

int suma(int numero){
	/*función creada para obtener la raiz digital de un número
	*se denominó suma porque se suman los digitos de un número que infrese el usuario
	*/
	int suma=0;//suma empieza en 0
	
	
	if (numero==0){//si el numero ingresado es 0 entonces va a devolver el mismo número
		 return(0);}
	if (numero>=1 && numero<=9){//si el numero es natural y menor que 10, también va a devolver el mismo número
		 return(numero);}
	if (numero<0){// si el nuemro es menor a 0, va a aparecer error en la pantalla y no va a sumar los digitos, sino que va a imprimir un 0
		printf("ERROR\nTiene que ser natural");}
	else
		while(numero>0){// si el numero es mayor a 0, incluyendo el mayor a 10 por que ya pasó por esa pregunta, va a ainiciar el ciclo
		suma = suma + (numero%10); //sumandole a 0 el resto del numero dividido por 10
		numero = numero/10;//como dividir por 10 le quita el último dígito, se convierte en un numero más pequeño
		}//este ciclo se va a rrepetir hasta que la división por 10 sea 0 
	
	while (suma > 9){ //se va a iniciar un nuevo ciclo siempre y cuando la suma sea mayor a 9
		suma = (suma%10) + (suma/10); //si este es el caso, se extraerán los digitos de la suma y se sumarán, así obtener la raiz digital final del número original
	}
		
	
	return  suma;//y devolverá el resultado de la suma
	
}
	
int main(){
    int raiz; //el nombre que va a obtener la función "suma"
    long numero;  // se ocupa long en caso de que el numero sea largo
    printf("Para calcular la raiz digiltal escriba un numero natural: \n");// aquí se imprime la petición del número
	scanf("%ld", &numero);// y se pide el número
	raiz = suma(numero); // se va a llamar a la función "suma" para ocuparla con el número que ingreśo el usuario
	printf("\nLa raiz digital de %ld es %d \n", numero, raiz);// e imprime el resultado ocupando el nuevo nombre de la función
		
	return  0;// no retorna nada y termina el programa
}
