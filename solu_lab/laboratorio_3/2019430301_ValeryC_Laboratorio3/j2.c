#include <stdio.h>


void numero_mayor(int numero_inicial){ // se ocupa el void porque la función solo hará un cálculo e imprimirá el resultado, no devolverá una variable
	int numero2, numero3, numero4, primer_resultado, segundo_resultado, tercer_resultado, n1, n2, n3, n4;
	
	primer_resultado=(numero_inicial/1000);//primer_resultado=primera cifra
	numero2 = numero_inicial-(primer_resultado*1000);//numero2=los otros tres digitos
	segundo_resultado=(numero2/100);//segundo_resultado= la segunda cifra
	numero3 = numero2-(segundo_resultado*100);//numero3= las ultimas 2 cifras
	tercer_resultado=(numero3/10);//tercer_resultado= la tercera cifra
	numero4 = numero3-(tercer_resultado*10);//numero4= ultima cifra
	
	n1=primer_resultado; //los nombres de las cifras obtenidas se cambiaron por nombres más cortos para facilitar el trabajo
	n2=segundo_resultado; 
	n3=tercer_resultado; 
	n4=numero4;
	/* en los siguientes if anidados se probaran todas las opciones posibles
	 * en que 4 números diferentes se pueden ordenar de mayor a menor
	 */
	 if(n1>n2){    //si el primero es mayor que el resto
		 if(n2>n3 && n2>n4){
			 if(n3>n4){
				 printf("el número mayor que se puede formar es: %d%d%d%d\n", n1, n2, n3, n4);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n1, n2, n4, n3);}}
	if(n1>n3){
		if (n3>n2 && n3>n4){
			if(n2>n4){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n1, n3, n2, n4);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n1, n3, n4, n2);}}
	if(n1>n4){
		if (n4>n2 && n4>n3){
			if(n2>n3){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n1, n4, n2, n3);}
			else
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n1, n4, n3, n2);}}
	if(n2>n1){     // si el segundo es mayor que el resto
		 if(n1>n3 && n1>n4){
			 if(n3>n4){
				 printf("el número mayor que se puede formar es: %d%d%d%d\n", n2, n1, n3, n4);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n2, n1, n4, n3);}}
	if(n2>n3){
		if (n3>n1 && n3>n4){
			if(n1>n4){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n2, n3, n1, n4);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n2, n3, n4, n1);}}
	if(n2>n4){
		if (n4>n1 && n4>n3){
			if(n1>n3){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n2, n4, n1, n3);}
			else
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n2, n4, n3, n1);}}
	if(n3>n1){    // si el tercero es mayor que el resto
		 if(n1>n2 && n1>n4){
			 if(n2>n4){
				 printf("el número mayor que se puede formar es: %d%d%d%d\n", n3, n1, n2, n4);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n3, n1, n4, n2);}}
	if(n3>n2){
		if (n2>n1 && n2>n4){
			if(n1>n4){
				printf("el número mayor que se puede formar es: n%d%d%d%d\n", n3, n2, n1, n4);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n3, n2, n4, n1);}}//poner ; en TODO
	if(n3>n4){
		if (n4>n1 && n4>n2){
			if(n1>n2){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n3, n4, n1, n2);}
			else
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n3, n4, n2, n1);}}
	if(n4>n1){    // si el cuarto es mayor que el resto
		 if(n1>n2 && n1>n3){
			 if(n2>n3){
				 printf("el número mayor que se puede formar es: %d%d%d%d\n", n4, n1, n2, n3);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n4, n1, n3, n2);}}
	if(n4>n2){
		if (n2>n1 && n2>n3){
			if(n1>n3){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n4, n2, n1, n3);}
			else 
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n4, n2, n3, n1);}}
	if(n4>n3){
		if (n3>n1 && n3>n2){
			if(n1>n2){
				printf("el número mayor que se puede formar es: %d%d%d%d\n", n4, n3, n1, n2);}
			else
			printf("el número mayor que se puede formar es: %d%d%d%d\n", n4, n3, n2, n1);}}
	
	
	} 

void numero_menor(int numero_inicial){
	int numero2, numero3, numero4, primer_resultado, segundo_resultado, tercer_resultado, n1, n2, n3, n4;
	
	primer_resultado= numero_inicial/1000;//primer_resultado=primera cifra
	numero2=numero_inicial-(primer_resultado*1000);//numero2=los otros tres digitos
	segundo_resultado=numero2/100;//segundo_resultado= la segunda cifra
	numero3=numero2-(segundo_resultado*100);//numero3= las ultimas 2 cifras
	tercer_resultado=numero3/10;//tercer_resultado= la tercera cifra
	numero4=numero3-(tercer_resultado*10);
	
	n1=primer_resultado;
	n2=segundo_resultado;
	n3=tercer_resultado;
	n4=numero4;
	/* en los siguientes if anidados se probaran todas las opciones posibles
	 * en que 4 números diferentes se pueden ordenar de menor a mayor
	 */
	 /*se ocupa la misma logica de lafunción anterior pero con el orden final al revés
	  */
	if(n1>n2){  //si el primero es mayor
		 if(n2>n3 && n2>n4){
			 if(n3>n4){
				 printf("el número menor que se puede formar es: %d%d%d%d\n", n4, n3, n2, n1);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n3, n4, n2, n1);}}
	if(n1>n3){
		if (n3>n2 && n3>n4){
			if(n2>n4){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n4, n2, n3, n1);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n2, n4, n3, n1);}}
	if(n1>n4){
		if (n4>n2 && n4>n3){
			if(n2>n3){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n3, n2, n4, n1);}
			else
			printf("el número menor que se puede formar es: %d%d%d%d\n", n2, n3, n4, n1);}}
	if(n2>n1){ // si el segundo es mayor
		 if(n1>n3 && n1>n4){
			 if(n3>n4){
				 printf("el número menor que se puede formar es: %d%d%d%d\n", n4, n3, n1, n2);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n3, n4, n1, n2);}}
	if(n2>n3){
		if (n3>n1 && n3>n4){
			if(n1>n4){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n4, n1, n3, n2);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n1, n4, n3, n2);}}
	if(n2>n4){
		if (n4>n1 && n4>n3){
			if(n1>n3){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n3, n1, n4, n2);}
			else
			printf("el número menor que se puede formar es: %d%d%d%d\n", n1, n3, n4, n2);}}
	if(n3>n1){ // si el tercero es mayor
		 if(n1>n2 && n1>n4){
			 if(n2>n4){
				 printf("el número menor que se puede formar es: %d%d%d%d\n", n4, n2, n1, n3);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n2, n4, n1, n3);}}
	if(n3>n2){
		if (n2>n1 && n2>n4){
			if(n1>n4){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n4, n1, n2, n3);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n1, n4, n2, n3);}}
	if(n3>n4){
		if (n4>n1 && n4>n2){
			if(n1>n2){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n2, n1, n4, n3);}
			else
			printf("el número menor que se puede formar es: %d%d%d%d\n", n1, n2, n4, n3);}}
	if(n4>n1){  // si el cuarto es mayor
		 if(n1>n2 && n1>n3){
			 if(n2>n3){
				 printf("el número menor que se puede formar es: %d%d%d%d\n", n3, n2, n1, n4);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n2, n3, n1, n4);}}
	if(n4>n2){
		if (n2>n1 && n2>n3){
			if(n1>n3){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n3, n1, n2, n4);}
			else 
			printf("el número menor que se puede formar es: %d%d%d%d\n", n1, n3, n2, n4);}}
	if(n4>n3){
		if (n3>n1 && n3>n2){
			if(n1>n2){
				printf("el número menor que se puede formar es: %d%d%d%d\n", n2, n1, n3, n4);}
			else
			printf("el número menor que se puede formar es: %d%d%d%d\n", n1, n2, n3, n4);}}
		
}


int main()
{
	
	int numero;// numero es el nombre de la variable del número que se va a ingresar
	
	printf("escriba un numero de 4 cifras DIFERENTES:\n");// en esta parte se imprime la petición con la palabra DIFERENTES destacada para que no coloquen números con digitos que se repitan 
	scanf("%d", &numero);//se pide el número
	numero_mayor(numero); //se llama a ala función numero_mayor para tarabajar al número ingresado e imprimir el n° mayor que se puede formar
	numero_menor(numero);//se llama a ala función numero_menor para tarabajar al número ingresado e imprimir el n° menor que se puede formar
	
	
	return  0; //no retorna nada y termina el programa
}
