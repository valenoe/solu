/*Aqui se muestran versiones iterativas y recursivas de las funciones
suma_digitos y raiz_digital*/
/*Se adjunta un programa de prueba*/

#include <stdio.h>

int suma_digitos_rec (int n)
/*Calcula y devuelve la suma de los digitos de un numero, de forma recursiva*/
{
	if (n==0)
		return(0);
	else
		return ((n%10) + suma_digitos_rec(n/10));
}


int suma_digitos_iter(int n)
/*Calcula y devuelve la suma de los digitos de un numero, de forma iterativa*/
{
	int suma=0;

	while(n>0)
	{
		suma+=(n%10);
		n/=10;
	}
	
	return (suma);
}


int raiz_digital_iter (int n)
/*Calcula y devuelve la raiz digital de un numero, de forma iterativa*/
{
	while(n>9)/*El numero tiene mas de una cifra*/
		n=suma_digitos_rec(n);

	return(n);
}


int raiz_digital_rec1(int n)
/*Calcula y devuelve la raiz digital de un numero, de forma recursiva*/
/*Utiliza la funcion suma_digitos*/
{
	if(n<=9)
		return(n);

	else
		return(raiz_digital_rec1(suma_digitos_rec(n)));
}


int raiz_digital_rec2(int n)
/*Calcula y devuelve la raiz digital de un numero, de forma recursiva*/
/*No utiliza la funcion suma_digitos*/
{
	if(n<=9)
		return(n);
	
	else
		return(raiz_digital_rec2((n%10) + raiz_digital_rec2(n/10)));
}


int main()
{
	int a;

	/*Pide el numero*/
	printf("\nIntroduzca un numero:\n\n");
	scanf("%d%*c",&a);

	/*Antes comprueba si es un numero natural*/
	if(a<0)
		printf("\nError: numero negativo\n");

	else
	{
		printf("\nLa suma de sus digitos es %d\n", suma_digitos_rec(a));
		printf("\nLa suma de sus digitos es %d\n", suma_digitos_iter(a));
		printf("\nSu raiz digital es %d\n", raiz_digital_iter(a));
		printf("\nSu raiz digital es %d\n", raiz_digital_rec1(a));
		printf("\nSu raiz digital es %d\n", raiz_digital_rec2(a));
	}
		
}
