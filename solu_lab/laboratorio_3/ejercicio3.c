#include <stdio.h>
#include <math.h>

float raiz(int numero){//funcion creada para obtener la raiz cubica de un numero ingresado por el usuario
	
	float raiz;
	raiz=0.1;//la raiz empieza por 0.1
	while((raiz*raiz*raiz)<numero){
		raiz = raiz+(0.001);}//para luego sumarle 0.001
	
	return raiz;
	
}

int main(){
	
	int numero;
	
	printf("ingrese un numero:");
	scanf("%d", &numero);
	printf("la raiz cubica del numero %d es: %.2f", numero, raiz(numero));
	
	return  0;
	
}
