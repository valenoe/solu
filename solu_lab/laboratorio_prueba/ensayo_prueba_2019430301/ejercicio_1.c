#include <stdio.h>
#include <string.h>


int main(){
	//matrices para comparar el nombre o palabra ingresada con las letras en orden y las letras cifradas 
	char nombre[25], nuevo_nombre[25];
	char alfabeto_normal[26]= {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	char alfabeto_cifrado[26]= {'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'A', 'B', 'C'};
    char tilde[34] = {"áéíóúü,.-_;:{}()°[]ñ¿¡!?"}; //cadena para comparar con tildes y otras cosas, el tamaño inclulle 
	printf("ingrese un nombre de maximo 25 caracteres:\n");
	gets(nombre);//recibe la frase
	//ciclo para comparar con tildes
	for(int i=0; i<25; i++){
		for(int j=0; j<34; j++){
			if(nombre[i]==tilde[j]){
				nuevo_nombre[i] = tilde[j]; //guarda los tildes que existen
			}
		}
	}	
	//ciclo general
	for(int i=0; i<25; i++){
		for(int j=0; j<26; j++){
			//compara la frase con el alfabeto normal y lo cambia cuando encuentra una coincidencia
			if(nombre[i] == alfabeto_normal[j]){
				nuevo_nombre[i] =  alfabeto_cifrado[j];
			}
			//si es que hay un espacio lo guarda 
			if(nombre[i] == 32){
				nuevo_nombre[i] = 32;
			}
			//si es que temrnina tambien lo guarda
			if(nombre[i] == '\0'){
				nuevo_nombre[i] = '\0';
			}
		}	
	} 
	//imprime la frase cifrada
	printf("el nombre es: %s\nel nombre cifrado es: %s", nombre, nuevo_nombre);
	return 0;
}
