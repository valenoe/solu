#include <stdio.h>
#include <time.h>
#include <stdlib.h>
//funcion que imprime las matrices llamadas
void imprime(int M[3][3]){
	
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){	
			printf("-%d", M[i][j]);
        }
        printf("-\n");
	}
}
int main(){
	//semilla del tiempo
	srand(time(0));
	
	int	matriz[3][3], otra[3][3];
	//ciclo para rellenar las matrices originales  
	for(int i=0; i<3; i++){
		for(int j=0; j<3; j++){	
			matriz[i][j]= (rand()%3)+1;
			otra[i][j]= (rand()%3)+1;
        }
	}
	int  k, i, j, resul[3][3], temp=0;
	//ciclo de i para recorrer hacia abajo la primera matri y la resultante 
	for(i=0; i<3; i++){
		for(k=0; k<3; k++){	//k para recorrer hacia el lado la segunda y la resultante
				temp=0;
			for(j=0; j<3; j++){ //l para recorrer hacia el lado la primera y hacia abajo la segunda
			    temp= temp + matriz[i][j]*otra[j][k];
			    resul[i][k] =temp;
			}
		}
	}
	printf("primera matrix\n");
	imprime(matriz);
	printf("\nsegunda matrix\n");
	imprime(otra);
	printf("\nmultiplicación\n");
	imprime(resul);	
	return 0;
}

