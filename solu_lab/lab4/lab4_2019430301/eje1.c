#include <stdio.h>
#include <stdlib.h>

int ULAM(int num){
		
	if(num>1){//si el numero es mayor a uno se ejecuta ULAM
		if(num%2==0){//si es par se divide por dos
			num = num/2;
			printf("%d\n", num);//imprime el numero actual
			ULAM(num);//vuelve a realizar la funcipon
			}
	
		else{
			num = num*3 + 1;//si es impar se multiplica por 3 y se le resta 1
			printf("%d\n", num);//imprime el numero actual
			ULAM(num);}//vuelve a realizar la función
}
	else{//si el nuemro es 1, no retorna nada
		return 0;}
		
	return num;
}

int main(){
	 
	int num;
	
	printf("escriba un numero positivo:\n");
	scanf("%d", &num);
	//pide el número y ejecuta la función
	ULAM(num);
	
	return  0;
}
