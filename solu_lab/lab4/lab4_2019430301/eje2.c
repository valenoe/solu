#include <stdio.h>

int raiz(int num){
	
	if(num==0){//si el nuemro es 0, va a devolvervel numero 
		return(0);}
	if(num<10){//si es menor a 10 tambien
		return(num);}
	if(num<0){//si es menor a 0 va a imprimir error
		return(printf("     ERROR       \nNumero natural dije"));}
	else{//si no es ninguna de las anteriores va a ejecutar el procedimiento de obtener los digitos y sumarlos
		return(raiz((num%10)+raiz(num/10)));}
}

int main(){
	
	int numero;
	
	printf("       bienvenido a raiz digital       \n");
	printf("Para continuar escriba un número natural:\n");
	scanf("%d", &numero);
	
	printf("la raiz digital de %d es: %d", numero, raiz(numero));// se imprimen los resultados
	
	return  0;
}
	
