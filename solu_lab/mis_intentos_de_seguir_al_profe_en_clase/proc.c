#include <stdio.h>

int proc(int valor){
	int u, suma;
	suma=0;
	
	for(u=0;u<100;u++){
		
		if(u%valor==0){
			suma=suma+u;
		}
	}
	return suma;
}

int main(){
	
	int numero, sumadiv;
	printf("ingrese un numero\n");
	scanf("%d",&numero);
	sumadiv=proc(numero);
	printf("la suma de todos los divisores de %d entre 0 y 99 es: %d",numero, sumadiv);
	return  0;
}
	
