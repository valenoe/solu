#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
int ATGC(int nucleotidos, int i, int j, char ADN[i][j] ){
	
	char nucle;
	//se dan letras correspondientes a nucleotidos a los numeros llamados en la funcion
	switch(nucleotidos){
					case 0: 
						ADN[i][j] = 'a';
						nucle = ADN[i][j];
						break;
					case 1: 
						ADN[i][j] = 't';
						nucle = ADN[i][j];
						break;
					case 2:
						ADN[i][j] = 'g';
						nucle = ADN[i][j];
						break;
					case 3:
						ADN[i][j] = 'c';
						nucle = ADN[i][j];
						break;
					}
	
	
	return nucle; //devuelve el nucleotido correspondiente
}
int main(){
	
	srand(time(0));
	
	int j, i, M, N, nucleotidos; 
	N=16;
	M=60;
	//se crea la matriz con los valores del ejemplo
	char ADN[N][M];
	
	//le da nucleotidos a los numeros aleatorios llamando a la funcion definida anteriormente
	for(i=0 ;i<N ;i++){
			for(j=0 ;j<M;j++){
				nucleotidos = rand()%4;
				ADN[i][j] = ATGC(nucleotidos, i, j, ADN);
				}
				printf("\n");
			}
	//imprime la matriz completa
	for(i=0 ;i<N ;i++){
			for(j=0 ;j<M;j++){
				printf(" %c", ADN[i][j]);
			}
			printf(" \n");
		}
	//se pide la cadena y calcula el largo
	char cadena[15];		
	int largo_cadena;
	printf("ingrese la cadena que quiere buscar (maximo 15 caracteres): ");
	scanf("%s", cadena);
	
	largo_cadena = strlen(cadena);
	printf("largo cadena: %d", largo_cadena);	
		   
	
	
   
   return 0;
}
