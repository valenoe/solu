#include <stdio.h>

int main (){
	
	int N;
	
	//le pide al usuari que ingrese el rango de los arreglos 
	printf("ingerese el limite de los arreglos: "); 
	scanf("%d", &N);
	
	//se declara la matris con el rango
	int A[N][N]; 
	int i, j;
	
	//se le asignan valores a los indices
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(i==j){
				//si i y j son iguales, se asigna 1, ya que será la diagonal 
				A[i][j]= 1;
			}
			else{
				//si no, se le asigna 0
				A[i][j]= 0;	
			}
		}
	}
	
	//imprime la matriz completa
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			printf("|%d", A[i][j]);
		}
		printf("|\n"); //ya que el valor maximo de i y j son el mismo, se va a ver como un cuadrado
	}
	
	return 0;
}
