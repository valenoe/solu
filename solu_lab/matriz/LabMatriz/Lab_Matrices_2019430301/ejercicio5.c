#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	
	srand(time(NULL));
	
	int largo, ancho, rango;
	//se piden los datos necesarios para hacer la matriz
	printf("ingrese los sigientes datos para su matriz:\nlargo:");
	scanf("%d", &largo);
		//se evalua que cumpla con las condiciones para continuar con los pasos
	if (largo<1 || largo>50){
		printf("tiene que estar en el rango de 1 a 50");
	}
	else{
		printf("ancho:");
		scanf("%d", &ancho);
		if(ancho<1 || ancho>30){
			printf("tiene que estar en el rango de 1 a 30");
		}
		else{
			printf("rango:");
			scanf("%d", &rango);
	//si pasa todo se crea la matriz con sus datos
			int matriz[ancho][largo];
			int i, j;
		//se rellena con números aleatorios
			for(i=0; i<largo; i++){
				for(j=0; j<ancho; j++){
					matriz[i][j] = (rand()%rango)+1;
				}
			}
			printf("\n");
			//se imprime completa
			for(i=0; i<largo; i++){
				for(j=0; j<ancho; j++){
					printf("|%d", matriz[i][j]);
				}
				printf("|\n");
			}
			printf("\n");
		
		//se crea la matriz transpuesta
			int zirtam[largo][ancho];
			//se llena con los datos de los indices contrarios a los de la matriz original y se imprime
			for(i=0; i<ancho; i++){
				for(j=0; j<largo; j++){
					zirtam[i][j] = matriz[j][i];
					printf("|%d", zirtam[i][j]);
				}
				printf("|\n");
			}
		}  
	}
	return  0;
}
