#include <stdio.h>


int main (){
	
	int N;
	//advertencia
	printf("de ustede depende cuanto tiempo este asignado números, no coloque nu numero muy grande en la siguiente linea");
	//petición del tamaño
	printf("\ningrese el limite de los arreglos: ");
	scanf("%d", &N); 
	
	//se declara la matriz con el tamaño
	int A[N][N]; 
	int i, j, contador_simetrica = 0, numeros;
	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
				printf("ingrese los numeros que iran en el indice (%d,%d):", i, j);
				scanf("%d", &numeros);
				A[i][j] = numeros;
		}
	}	
	
	//se compara si es que el valor de un ince es igual al del indice simetrico respecto a la diagonal principal
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(j!=i){
				if(A[i][j] == A[j][i]){
					contador_simetrica++;
					// si es que es así, el contador aumenta
				}
			}
		}
	}	
	
	
	
	//imprime la matriz completa
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			printf(" %d", A[i][j]);
		}
		printf("\n"); 
	}
	
	if(N == 1){
		printf("no se puede decir wue esto es simétrico o no ya que solo está en una dimensión");
	}
	else{
		//con la siguiente funcion se obtiene el resultado teorrico de cuantos números son iguales al números que contiene su índice simetrico
		int si_es = (N*N)-N;
		//y se compara con el resultado real, indicando si es que es o no simétrica  
		if(contador_simetrica == si_es){
			printf("es simetrica");
		}
		else{
			printf("no es simetrica");
		}
	}
	return 0;
}

