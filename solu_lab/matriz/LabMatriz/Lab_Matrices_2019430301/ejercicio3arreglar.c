#include <stdio.h>
#include <stdlib.h>

//~ int suma(int i, int N, int A[][]){
	
	//~ int j, adicion = 0;
	
	//~ for(j=0; j<N; j++){
			//~ adicion = adicion + A[i][j];
		
	//~ //	printf(" %d", suma);
	//~ }
	//~ return suma;
//~ }

int main (){
	
	int N;
	
	//le pide al usuario que ingrese el rango de los arreglos 
	printf("ingerese el limite de los arreglos: "); 
	scanf("%d", &N);
	
	//se declara la matris con el rango
	int A[N][N]; 
	int i, j, num, suma1, suma2;
	
	//le asigna el valor que ingrese el usuario a cada uno de los indices
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			printf("agregue valores a la posición (%d,%d): ", i, j);
			scanf("%d", &num);
				A[i][j]= num;
		}
	}	
	//imprime la matriz completa
	printf("\nla matriz completa\n");
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			printf(" %d", A[i][j]);
		}
		printf("\n"); //ya que el valor maximo de i y j son el mismo, se va a ver como un cuadrado
	}
	
	//imprime la suma de las filas
	printf("\nsuma por fila: \n");
	suma1=0;	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			suma1 = suma1 + A[i][j];
		}
		printf(" %d", suma1);
		printf("\n"); 
		suma1=0; //vuelve el valor de suma1 a 0 para que la siguiente fila empieze dese 0 y no contando la sumatoria anterior
	}
	
	//imprime la suma de todo
	suma2=0;
	printf("\nsuma total: ");	
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			suma2 = suma2 + A[i][j];
		}
	}
	printf("%d", suma2);
		
	 
	
	return 0;
}

