#include <stdio.h>


int main(){
	
	int matriz[4][5];
	int i, j;
	
	printf("matriz de 4x5\n");
	//pide los numeros de cada indice

	
	for(i=0 ;i<4 ;i++){
		for(j=0 ;j<5;j++){
			printf("ingrese un numero para el indice(%d,%d): ", i, j);
			scanf("%d", &matriz[i][j] );
		}
	}
	
	printf("\nmatriz\n");
	//se imprime la matriz
	for(i=0 ;i<4 ;i++){
		for(j=0 ;j<5;j++){
			printf("|%d|", matriz[i][j]);	
		}
		printf("\n");
	}
	
	int nuevo, z;
	int reglon[4];
	nuevo=0;
	z=0;
	//suma las filas y las mete al un arreglo llamado reglon
	while(z<4){
		for(i=0 ;i<4 ;i++){
			for(j=0 ;j<5;j++){
				nuevo= nuevo + matriz[i][j];	
			}
			reglon[z] = nuevo;
			z++;
			//convierte nuevo a 0 para que no influya en el siguiente digito de reglon 
			nuevo = 0; 
		}
	}
	//imprime el arreglo reglon
	printf("\nvector de 4 números\n");
	for(i=0 ;i<4 ;i++){
		printf("|%d|", reglon[i]);
	}
	
	return 0;
}
