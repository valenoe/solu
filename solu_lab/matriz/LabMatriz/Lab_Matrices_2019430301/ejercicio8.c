#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(){
	
	srand(time(0));
	
	int i, j, porte, rango;
	//pregunta el tamaño
	printf("ingrese los parametros de la matriz");
	scanf("%d", &porte);
	
	//se define la matriz con el dato pedido 
	int matriz[porte][porte];
	
	//se pide un rango para llenar la matriz con numeros entre 0 y el ra4ngo
	printf("ingrese un rango: ");
	scanf("%d", &rango);
	for(i=0 ;i<porte ;i++){
		for(j=0 ;j<porte;j++){
			matriz[i][j] = rand()%rango;
		}
	}
	
	printf("\nmatriz original\n");
	//se imprime la matriz
	for(i=0 ;i<porte ;i++){
		for(j=0 ;j<porte;j++){
			printf("|%d|", matriz[i][j]);	
		}
		printf("\n");
	}
	
	printf("\n");
	
	//imprime la nueva matriz invirtiendo el orden de las filas
	printf("\nnueva matriz\n");
	for(i=porte-1; i>-1; i--){
		for(j=0 ;j<porte;j++){
			printf("|%d|", matriz[i][j]);	
		}
		printf("\n");
	}
	
	return 0;
}
			
