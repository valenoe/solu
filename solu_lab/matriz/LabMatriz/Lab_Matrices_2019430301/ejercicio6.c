#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(){
	
	
	
	int tiempo[7][24];
	int i, j;

	//printf("temperatura esta semana:\n");
	printf("ingrese datos de temperatura para:\n");
	//advertencia
	printf("(considere que los datos deben ser logicos y pueston en °C\nsi no, los resultados no seran coherentes)");
	//petición de datos para cada hora del día, empezando por el día 1
	for(i=0; i<7; i++){
		//printf("\ndia: %d\n", i+1);
		for(j=0; j<24; j++){
		//	printf("hora %d: ",j);
			//scanf("%d", &tiempo[i][j]);
			
			tiempo[i][j] = rand()%40;
			
		}
		//printf("\n\n");
	} 
	printf("\n\n");
	
	//impresión de la matriz tiempo
	for(i=0; i<7; i++){
		printf("dia %d: " , i+1);
		for(j=0; j<24; j++){
			printf("|%d|", tiempo[i][j]);
		}
		printf("\n");
	}
	
	//se busca la menor temperatura comparandola con el número del índice 0,0
	int menor;
	menor = tiempo[0][0];
	for(i=0; i<7; i++){
		for(j=0; j<24; j++){
			if (menor > tiempo[i][j]){
				menor = tiempo[i][j];
			}
		}
	}
	
	printf("\nmenor temperatura de la semana: %d °C\n", menor);
	
	for(i=0; i<7; i++){
		menor = tiempo[i][0];
		for(j=0; j<24; j++){
			if (menor > tiempo[i][j]){
				menor = tiempo[i][j];
			}
		}
		printf("la menor temperatura del día %d es: %d °C\n", i, menor);
	}
	//imprime la menor temperatura
	
	
	//se busca la mayor con el mismo metode de temperatura menor
	int mayor;
	mayor = tiempo[0][0];
	
	for(i=0; i<7; i++){
		for(j=0; j<24; j++){
			if (mayor <= tiempo[i][j]){
				mayor = tiempo[i][j];
			}
		}
	}
	printf("la temperatura mayor del día %d es: %d °C\n",i, mayor);	
	

	for(i=0; i<7; i++){
		mayor = 0;
		for(j=0; j<24; j++){
			if (mayor <= tiempo[i][j]){
				mayor = tiempo[i][j];
			}
		}
	printf("la temperatura mayor del día %d es: %d °C\n",i, mayor);	
	}
	//se imprime la temperatura mayor
	
		
	
	float prom_semanal;
	float media; 
	//se busca el promedio de temperaturas de toda la semana
	for(i=0; i<7; i++){
		for(j=0; j<24; j++){
			//para ello se suman las temperaturas
			media = media + tiempo[i][j];
		}
		
	}
	//y se dividen por el total de datos
	prom_semanal= media/(7*24);
	//luego se imprime
	printf("\npromedia de temperatura de la semana: %0.2f °C\n", prom_semanal);
	
	
	float prom;
	int contador_temp_mayor=0;
	media= 0;
	prom=0.0;

	// se calcula el promedio de temperaturas diario
	for(i=0; i<7; i++){
		for(j=0; j<24; j++){

	//se suman las temperaturas del día
			media = media + tiempo[i][j];
		}
	//se dividen por el total de horas
		prom = media/24;
		if(prom > 30){
	//el contador aumenta cuando el promedio de temperaturas del dia supera los 30 °C
			contador_temp_mayor++;
		}
	//imprime latemperatura del día
		printf("promedio de temperatura del dia %d: %.2f°C\n",i+1, prom);
	//qmedia vuelve a ser 0 para que no influya en el día siguiente
		media=0;
		
	}
	//imprime cuantos días de la semana tubieron una temperatura promedio mayor a 30 °C
	printf("\ndias en que el promedio de temperatura diario fua mayor a 30 °C: %d",contador_temp_mayor);
			

	return  0;
}
