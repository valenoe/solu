#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main (){
	
	srand(time(NULL));
	
	int N, opcion;
	
	printf("ingrese el limite de los arreglos es: ");
	scanf("%d", &N);
	
	printf("escoja: \n1)usted quiere poner los numeros\n2)quiere numeros random: \n");
	scanf("%d", &opcion); 
	
	if(opcion < 1 || opcion > 2){
		printf("tienen que ser los nuemros 1 o 2");
	}
	else{
	
		//se declara la matriz con el rango
		int A[N][N]; 
		int i, j, simetrica=0, rango;
	
		//le pide al usuario que ingrese el rango de los arreglos 
		
		printf("\n");
	
	
		if(opcion == 1){
			int num;
			for(i=0; i<N; i++){
				for(j=0; j<N; j++){
					printf("ingrese los numeros que van a ir en el indice (%d,%d): ", i,j );
					scanf("%d", &num);
					A[i][j]= num;
				}
			}
		}
		
		else if(opcion == 2){
			for(i=0; i<N; i++){
				for(j=0; j<N; j++){
					A[i][j]= rand()%rango;
				}
			}
			printf("ingrese el rango de los numeros que van a ir en la matriz: ");
			scanf("%d", &rango);
		}
	
			
	
		//se le asigna un 0 a los indices
		for(i=0; i<N; i++){
			for(j=0; j<N; j++){
				if(j!=i){
					if(A[i][j] == A[j][i]){
						simetrica++;
					}
				}
			}
		}	
	
	
	
		//imprime la matriz completa
		for(i=0; i<N; i++){
			for(j=0; j<N; j++){
				printf(" %d", A[i][j]);
			}
			printf("\n"); //ya que el valor maximo de i y j son el mismo, se va a ver como un cuadrado
		}
	
		int si_es = N*2;
		if(simetrica == si_es){
			printf("es simetrica");
			}
	//~ else if(simetrica< si_es && simetrica>0){
		//~ printf("tiene algo de simetría, pero no lo es completo");
	//~ }
		else{
			printf("no es simetrica");
			}
	}
return 0;
}

