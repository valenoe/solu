#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
//función que imprime una matrizzz
void impresora(int N, int tablero[N][N]){
	for(int i=0; i<N;i++){
	for(int j=0; j<N;j++){
		printf("[%c]",tablero[i][j]);
		}
		printf("\n");}
	}
//función que de manera aleatoria define el salto de la rana
void saltos(int N, int tablero[N][N], int x, int y, int fuera, int n_saltos, int repetidos){
	int invertir[2]={1, -1};
	int p, aux, auy;
	printf("La rana se encuentra en el punto (%d, %d) ", x, y);
	p=invertir[rand()%2];
	//se define el punto al que saltará la rana, entre 0 y 3 espacios desde la posición actual
	aux=x+p*rand()%3;
	auy=y+p*rand()%3;
	//condiciones para contabilizar si la rana saltaría fuera de la cuadricula
	if(aux>N||auy>N){
		aux=x;
		auy=y;
		fuera++;}
	if(aux<0){
		aux=aux*-1;}
	if(auy<0){
		auy=auy*-1;}
	if(aux<0||auy<0){
		fuera++;}
		//condicion para contabilizar si la rana salta en el mismo espacio
	if(aux==x&&auy==y){
		repetidos++;}
	printf("y se moverá al punto (%d, %d)\n", aux, auy);
	//se le otorga el valor que representa a la rana en un espacio
	tablero[auy][aux]='0';
	//se imprime el nuevo tablero
	impresora(N, tablero);
	//se contabiliza el numero del salto
	n_saltos++;
	//se imprimen los datos pedidos por el ejercicio
	printf("Salto número: %d\n", n_saltos);
			printf("Cantidad de veces que la rana intentó saltar fuera: %d\n", fuera);
					printf("Cantidad de veces que la rana saltó en la misma posición: %d\n", repetidos);
		//se recursiona si aún existen espacios en donde la rana no hubiera estado
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
		if(tablero[i][j]==' '){
		saltos(N, tablero, aux, auy, fuera, n_saltos, repetidos);}
		}}
	}
void dimensiones(){
	srand(time(0));
	//se decla variables, N=dimensiones de la matriz, x= eje x del punto, y= eje y del punto
	int N, x, y;
	//se decla contadores a utilizar
	int fuera=0, n_saltos=0, repetidos=0;
	do {
		N=0;
		//se pide a usuario la dimensión de la matriz
	printf("Ingrese las dimensiones del tablero, no puede ser menor a 3 o mayor a 50\n");
	scanf("%d", &N);}while(N<3||N>50);
	int tablero[N][N];
	//se llena la matriz con forma de tablero
	for(int i=0; i<N;i++){
		for(int j=0; j<N;j++){
			tablero[i][j]=' ';
		}}
	impresora(N, tablero);
	//se pide a usuario la posición inicial de la rana
	do {
	x=0, y=0;
	printf("Ingrese la posición inicial, recuerde que los valores deben estar\ndentro de la matriz previamente generada\n");
	printf("Eje x: ");
	scanf("%d", &x);
	printf("Eje y: ");
	scanf("%d", &y);}while(x>N-1||x<0||y>N-1||y<0);
	tablero[y][x]='0';
	impresora(N, tablero);
	//se mandan los datos a la función saltos que define las posiciones de la rana de manera aleatoria
	saltos(N, tablero, x, y, fuera, n_saltos, repetidos);
}
int main()
{
	dimensiones();
	return 0;
}
