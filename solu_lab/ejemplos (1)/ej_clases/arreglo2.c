#include<stdio.h>

int 
main () { 
  int arreglo_de_numeros[10], suma;    

  /* cargo array con valor igual al indice mas 10*/
  for (int i=0;i<10;i++)
    arreglo_de_numeros[i]=i+10;
  
  /* imprimo contenido del arreglo */
  for (int i=0;i<10;i++)
    printf("Elemento % d del array es % d\n",i,arreglo_de_numeros[i]);

  suma = arreglo_de_numeros[0]+ arreglo_de_numeros[2]+ arreglo_de_numeros[5];
  printf("La suma de los indices 0, 2 y 5 es: % d\n", suma );
        
  return 0;
}
