#include <stdio.h>
#include <stdlib.h>


//Se requiere un algoritmo para obtener un arreglo (C) de N elementos
//que contenga la suma de los elementos correspondientes de otros
//dos arreglos (A y B), la suma debe ser posición por posición.

int  main () { 
  
  int N;
  
  N = 3;
  
  int a[] ={1,2,3};
  int b[] ={2,3,4};
  
  int c[N];
  
  /*
  c[0] = a[0] + b[0] ;
  c[1] = a[1] + b[1] ;
  c[2] = a[2] + b[2] ;
  c[0] = a[0] + b[0] ;
  c[1] = a[1] + b[1] ;
  c[2] = a[2] + b[2] ;
  c[0] = a[0] + b[0] ;
  c[1] = a[1] + b[1] ;
  c[2] = a[2] + b[2] ;
  c[0] = a[0] + b[0] ;
  c[1] = a[1] + b[1] ;
  c[2] = a[2] + b[2] ;

  
  printf("valor de suma en indice 0  es: %d\n",c[0]);
  printf("valor de suma en indice 1  es: %d\n",c[1]);
  printf("valor de suma en indice 2  es: %d\n",c[2]); 
  */
  
  for (int i = 0; i < 3; i++){
	  c[i] = a[i] + b[i] ;
	  printf("valor de suma en indice %d  es: %d\n",i, c[i]);
	  }
        
  return 0;
}
