#include<stdio.h>

int 
main () { 
  int arreglo_de_numeros[10];     
  int i;
  /* cargo array con valor igual al indice mas 10*/
  for (i=0;i<10;i++)
   arreglo_de_numeros[i]=i+10;
  
  /* imprimo contenido del arreglo */
  for (int i=0;i<10;i++)
    printf("Elemento % d del array es % d\n",i,arreglo_de_numeros[i]);

  return 0;
}
