#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Se requiere un algoritmo para obtener un arreglo (C) de N elementos que contenga la suma de los elementos correspondientes de otros dos arreglos (A y B), la suma debe ser posición por posición.

int main(){

	int c[5];
	int a[5] = {1,2,3,4,5};
	int b[5] = {5,4,3,2,1};
	srand(time(0));
	
	c[0] = a[0] + b[0] ;
	c[1] = a[1] + b[1] ;
	c[2] = a[2] + b[2] ;
	c[3] = a[3] + b[3] ;
	c[4] = a[4] + b[4] ;
	
	printf("Indice 0, la suma es: %d\n",c[0]);
	printf("Indice 1, la suma es: %d\n",c[1]);
	printf("Indice 2, la suma es: %d\n",c[2]);
	printf("Indice 3, la suma es: %d\n",c[3]);
	printf("Indice 4, la suma es: %d\n",c[4]);
	printf("**************************\n");
	
	for (int i = 0; i < 5 ; i++){
		c[i] = rand()%5+1; 
		printf("Indice %d, la suma es: %d\n",i,c[i]);
	}
	
	return 0;
}
