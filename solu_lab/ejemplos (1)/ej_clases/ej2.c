#include <stdio.h>
#include <stdlib.h>


//Crea un arreglo donde se le indique el tamaño por teclado, crear una
//función que rellene el arreglo con los múltiplos de un numero pedido
//por teclado. Por ejemplo, si defino un arreglo de tamaño 5 y eligo un
//múltiplo de 3, el arreglo contendrá el 3, 6, 9, 12, 15.

void imprime(int X,  int arr[X]){
	
	int i;
	
    printf("Ahora vamos a imprimir los valores del arreglo en la función imprime\n");
    
    for (i = 0; i < X; i++)
	     printf("valor en indice %d  es: %d\n",i, arr[i]);
	
}


int  main () { 
  
  int N, multiplo, i;
  
  printf("Ingrese tamaño y multiplo \n");
  scanf("%d %d", &N, &multiplo);
  
  int arreglo [N];
  
  for (i = 0; i < N; i++){
	  arreglo [i] = multiplo * (i+1);
	  }
  
  imprime (N,arreglo);
  arreglo [3] = 54 ;
  imprime (N,arreglo);
  arreglo[4] = 1;
  imprime (N,arreglo);
  return 0;
}
